load 2-tuple.maude

load 3-tuple.maude

mod CONFIGURATION is
	sorts Attribute AttributeSet .
	subsort Attribute < AttributeSet .
	op none : -> AttributeSet  [ctor] .
	op _,_ : AttributeSet AttributeSet -> AttributeSet [ctor assoc comm id: none] .
	sorts Oid Cid Object Msg Portal Configuration .
	subsort Object Msg Portal < Configuration .
	op <_:_|_> : Oid Cid AttributeSet -> Object [ctor object format (ni!r o !r o !r o !r o)] .
	op none : -> Configuration [ctor] .
	op __ : Configuration Configuration -> Configuration [ctor config assoc comm id: none] .
	op <> : -> Portal [ctor] .
endm

view Oid from TRIV to CONFIGURATION is
	sort Elt to Oid .
endv

view Configuration from TRIV to CONFIGURATION is
	sort Elt to Configuration .
endv

fmod INT* is
pr INT .
sort Int* .
subsort Int < Int* .
op * : -> Int* .
endfm

view Int* from TRIV to INT* is
	sort Elt to Int* .
endv

view NatList from TRIV to META-LEVEL is
	sort Elt to NatList .
endv

view Term from TRIV to META-TERM is
	sort Elt to Term .
endv

view Substitution from TRIV to META-LEVEL is
	sort Elt to Substitution .
endv

mod MLM is
	pr SET{Oid} .
	pr STRING .
	pr QID .
	pr 2-TUPLE{NatList, Int*} .
	pr INT .
	pr FLOAT .
	pr META-LEVEL .

	sort System .
	op {_} : Configuration -> System .

	sort Name .
	subsort String < Name .
	op counter : -> Oid .
	op level : Nat -> Oid .
	op oid : Nat NatList -> Oid .
	op id : Nat NatList -> Name .
	op id : Nat String -> Name .

	sort Model .
	subsort Model < Cid .
	op Model : -> Model .
	op name`:_ : Name -> Attribute [prec 20 gather (&)] .
	op om`:_ : Name -> Attribute [prec 20 gather (&)] .
	op elts`:_ : Configuration -> Attribute [prec 20 gather (&) format (o o ++i --i)] .
	op rels`:_ : Configuration -> Attribute [prec 20 gather (&) format (o o ++i --i)] .

	sort Node .
	subsort Node < Cid .
	op Node : -> Node .
	op type`:_ : Name -> Attribute [prec 20 gather (&)] .
	op attributes`:_ : Configuration -> Attribute [prec 20 gather (&) format (o o ++i --i)] .

	sort Attri .
	subsort Attri < Cid .
	op Attri : -> Attri .
	op nameOrValue`:_ : Name -> Attribute [prec 20 gather (&)] .
	op nameOrValue`:_ : Int -> Attribute [prec 20 gather (&)] .
	op nameOrValue`:_ : Bool -> Attribute [prec 20 gather (&)] .
	op nameOrValue`:_ : Float -> Attribute [prec 20 gather (&)] .
	op nameOrValue`:_ : String -> Attribute [prec 20 gather (&)] .
	op type`:_ : Name -> Attribute [prec 20 gather (&)] .

	sort Relation .
	subsort Relation < Cid .
	op Relation : -> Relation .
	ops source`:_ target`:_ : Name -> Attribute [prec 20 gather (&)] .
	op min-mult`:_ : Nat -> Attribute [prec 20 gather (&)] .
	op max-mult`:_ : Int* -> Attribute [prec 20 gather (&)] .
	op type`:_ : Name -> Attribute [prec 20 gather (&)] .

	sort Counter .
	subsort Counter < Cid .
	op Counter : -> Counter .
	op value`:_ : Nat -> Attribute [prec 20 gather (&)] .

	vars NM Type Type' ActualType *Part : Name .
	vars O Oid O01 : Oid .
	vars Conf Elts Rels : Configuration .
	vars L N : Nat .
	vars Atts Atts01 Atts03 : AttributeSet .
	op * : Name Oid Name Configuration -> Bool .
	op *aux : Name Oid Name Configuration -> Bool .
	eq *(ActualType, level(L), Type, Conf)= Type == ActualType or-else *aux(ActualType, level(L), Type, Conf) .
	eq *aux(ActualType, level(s L), Type, < level(s L) : Model | elts : (< O01 : Node  | name : ActualType, type : Type', Atts01 > Elts), Atts > Conf)= *(Type', level(L), Type, Conf) .
	eq *aux(ActualType, level(s L), Type, < level(s L) : Model | rels : (< O01 : Relation | name : ActualType, type : Type', Atts03 > Rels), Atts > Conf)= *(Type', level(L), Type, Conf) .
	eq *aux(ActualType, Oid, Type, Conf) = false [owise] .

	sort BoxIndex .
	subsort Int* < BoxIndex .
	op box[_]{_} : BoxIndex Set{Oid} -> Oid .
	op boxes : Set{Oid} -> Object .

	vars N1 : Name .
	vars NV : Int .
	op getNumTokensInPlace : System Name -> Nat .

	eq getNumTokensInPlace({
       < level(4) : Model |
           elts : (
               < O01:Oid : Node | name : N1, type : id(3, "Place"), attributes :
                   < O02:Oid : Attri | nameOrValue : NV, Atts'':AttributeSet >   >
               Elts''':Configuration),
           Atts''':AttributeSet >
       Conf:Configuration },
       N1) = NV .
endm

mod UNBOXING is
	pr MLM .
	pr 2-TUPLE{Configuration,Set{Oid}} .

	var  S : System .
	vars N M L : Nat .
	var  I : Int .
	var  I* : Int* .
	vars Conf Elts Rels : Configuration .
	vars OS OS' OS'' : Set{Oid} .
	vars NOS : NeSet{Oid} .
	vars NL NL' : NatList .
	vars O O' : Oid .
	vars Atts Atts' Atts'' : AttributeSet .
	var NM : Name .
	var St : String .
	var BI : BoxIndex .

	op vOid : Oid NatList -> Oid .
	op vId : Name NatList -> Name .

	op vVar : AttributeSet NatList -> AttributeSet .
	op vVar : String NatList -> String .
	op vVar : Int NatList -> Int .
	op vVar : Configuration NatList -> Configuration .

	op unbox : Configuration -> Tuple{Configuration,Set{Oid}} .
	op unbox0 : Configuration Set{Oid} Set{Oid} -> Tuple{Configuration,Set{Oid}} .
	op unbox1 : Configuration NatList Set{Oid} Set{Oid} -> Tuple{Configuration,Set{Oid}} .
	op unbox2 : Configuration NatList Set{Oid} Set{Oid} -> Tuple{Configuration,Set{Oid}} .
	op cleanUpBoxes : Configuration Set{Oid} -> Configuration .
	op reconnect : Configuration NatList -> Configuration .
	op replicateVars : AttributeSet NatList -> AttributeSet .
	op replicateVars : Configuration NatList -> Configuration .

	eq unbox(Conf boxes(OS))
		 = (cleanUpBoxes(p1 unbox0(Conf, OS, empty), OS), p2 unbox0(Conf, OS, empty)) .
	eq unbox(Conf) = (Conf, empty) [owise] .

	eq unbox0(Conf, (box[N]{OS}, OS'), OS'')
		 = unbox0(p1 unbox1(Conf, N, OS, empty), OS', (p2 unbox1(Conf, N, OS, empty), OS'')) .
	eq unbox0(Conf, empty, OS) = (Conf, OS) .

	eq unbox1(Conf, NL s N, OS, OS')
		= unbox1(p1 unbox2(Conf, NL s N, OS, empty), NL N, OS, (p2 unbox2(Conf, NL s N, OS, empty), OS')) .
	eq unbox1(Conf, NL 0, OS, OS') = (Conf, OS') .

	eq unbox2(< level(L) : Model |
		elts : (< O : Node | name : NM, Atts > Elts), Atts' > Conf, NL, (O, OS), OS')
	= unbox2(< level(L) : Model |
		elts : (< vOid(O, NL) : Node | name : vId(NM, NL), replicateVars(Atts, NL) >
			< O : Node | name : NM, Atts > Elts), Atts' > Conf, NL, OS, OS') .
	eq unbox2(< level(L) : Model | rels : (< O : Relation | name : NM, Atts > Rels), Atts' > Conf, NL, (O, OS), OS')
		= unbox2(< level(L) : Model | rels : (< vOid(O, NL) : Relation | name : vId(NM, NL), replicateVars(Atts, NL) > < O : Relation | name : NM, Atts > Rels), Atts' > Conf, NL, OS, OS') .
	eq unbox2(Conf, NL N, (box[I]{OS}, OS'), OS'') = unbox2(Conf, NL N, (OS, OS'), (box[vVar(I, N)]{vOid(OS, N)}, OS'')) .
	eq unbox2(Conf, NL, empty, OS) = (reconnect(Conf, NL), OS) .

	op vOid : NeSet{Oid} NatList -> NeSet{Oid} .
	eq vOid((O, NOS), NL) = vOid(O, NL), vOid(NOS, NL) .
	eq vOid((box[I]{OS}, NOS), NL) = box[I]{vOid(OS, NL)}, vOid(NOS, NL) .
	eq cleanUpBoxes(Conf, (box[BI]{OS}, OS'))	= cleanUpBoxes(Conf, (OS', OS)) .
	eq cleanUpBoxes(< level(L) : Model | elts : (< O : Node | Atts > Elts), Atts' > Conf, (O, OS))
		 = cleanUpBoxes(< level(L) : Model | elts : Elts, Atts' > Conf, OS) .
	eq cleanUpBoxes(< level(L) : Model | rels : (< O : Relation | Atts > Elts), Atts' > Conf, (O, OS))
		 = cleanUpBoxes(< level(L) : Model | rels : Elts, Atts' > Conf, OS) .
	eq cleanUpBoxes(Conf, empty) = Conf .

	eq replicateVars((type : NM, Atts), NL) = (type : NM, replicateVars(Atts, NL)) .
	eq replicateVars((source : NM, Atts), NL) = (source : NM, replicateVars(Atts, NL)) .
	eq replicateVars((target : NM, Atts), NL) = (target : NM, replicateVars(Atts, NL)) .
	eq replicateVars((min-mult : N, Atts), NL) = (min-mult : N, replicateVars(Atts, NL)) .
	eq replicateVars((max-mult : I*, Atts), NL) = (max-mult : I*, replicateVars(Atts, NL)) .
	eq replicateVars((attributes : Conf, Atts), NL) = (attributes : replicateVars(Conf, NL), replicateVars(Atts, NL)) .
	eq replicateVars((none).AttributeSet, NL) = none .
	eq replicateVars(Atts, NL) = vVar(Atts, NL) [owise] .

	eq replicateVars(< O : Attri | nameOrValue : St, Atts > Conf, NL)
		 = < vOid(O, NL) : Attri | nameOrValue : vVar(St, NL), replicateVars(Atts, NL) > replicateVars(Conf, NL) .
	eq replicateVars(< O : Attri | nameOrValue : I, Atts > Conf, NL)
		 = < vOid(O, NL) : Attri | nameOrValue : vVar(I, NL), replicateVars(Atts, NL) > replicateVars(Conf, NL) .
	eq replicateVars((none).Configuration, NL) = none .
	eq replicateVars(Conf, NL) = vVar(Conf, NL) [owise] .

	eq reconnect(< level(L) : Model | rels : (< vOid(O, NL NL') : Relation | source : NM, Atts > Rels), elts : (< vOid(O', NL) : Node | name : vId(NM, NL), Atts' > Elts), Atts'' > Conf, NL)
		 = reconnect(< level(L) : Model | rels : (< vOid(O, NL NL') : Relation | source : vId(NM, NL), Atts > Rels), elts : (< vOid(O', NL) : Node | name : vId(NM, NL), Atts' > Elts), Atts'' > Conf, NL) .
	eq reconnect(< level(L) : Model | rels : (< vOid(O, NL NL') : Relation | target : NM, Atts > Rels), elts : (< vOid(O', NL) : Node | name : vId(NM, NL), Atts' > Elts), Atts'' > Conf, NL)
		 = reconnect(< level(L) : Model | rels : (< vOid(O, NL NL') : Relation | target : vId(NM, NL), Atts > Rels), elts : (< vOid(O', NL) : Node | name : vId(NM, NL), Atts' > Elts), Atts'' > Conf, NL) .
	eq reconnect(Conf, NL N) = reconnect(Conf, NL) [owise] .
	eq reconnect(Conf, nil) = Conf .
endm

mod UNBOX-RULE is
	pr META-LEVEL .
	pr CONVERSION .
	pr 2-TUPLE{Term,Term} .
	pr 2-TUPLE{Term,Substitution} .

	op error : -> [Term] .

	var MN : Header .
	var IL : ImportList .
	var SS : SortSet .
	var SSDS : SubsortDeclSet .
	var OPDS : OpDeclSet .
	var Mbs : MembAxSet .
	var Eqs : EquationSet .
	var RL : Qid .
	var Rls : RuleSet .
	var NL : NatList .
	var NNL : NeNatList .
	var AtS : AttrSet .
	vars Cond Cond' : Condition .
	var V : Variable .
	var C : Constant .
	var F : Qid .
	vars T T' T'' T1 T2 TN TM RHS LHS : Term .
	vars TL TL' : TermList .
	vars Subst Subst' : Substitution .
	vars N N' : Nat .
	var M : Module .
	var Ty : Type .

	op makeModule : Module Term Qid Substitution -> Module .
	ceq makeModule(M, T, RL, Subst)
	 = (mod MN is
				IL sorts SS . SSDS OPDS Mbs Eqs
				if fixBoxes(LHS, RHS, M, T, Subst) == ('none.Configuration, 'none.Configuration)
		 	  then none
		 		else crl '`{_`}[p1 fixBoxes(LHS, RHS, M, T, Subst)]
							=> '`{_`}[p2 fixBoxes(LHS, RHS, M, T, Subst)]
							if Cond
							/\ 'Conf':Configuration := p1 fixBoxes(LHS, RHS, M, T, Subst)
							/\ Cond'
							[label(RL) AtS] .
				fi
			endm)
	 if mod MN is
				IL sorts SS . SSDS OPDS Mbs Eqs
				(crl '`{_`}[LHS] => '`{_`}[RHS]
				if Cond /\ 'Conf':Configuration := T' /\ Cond' [label(RL) nonexec AtS] .
				Rls)
			endm := M .

	op fixBoxes : Term Term Module Term Substitution -> Tuple{Term,Term} .
	eq fixBoxes(LHS, RHS, M, T, Subst)
	  = if p2 fixBoxesLHS(LHS, M, T, Subst) == none
			then ('none.Configuration, 'none.Configuration)
			else (p1 fixBoxesLHS(LHS, M, T, Subst), fixBoxesRHS(RHS, p2 fixBoxesLHS(LHS, M, T, Subst)))
			fi .

	op fixBoxesLHS : Term Module Term Substitution -> Tuple{Term,Substitution} .
	op fixIdsLHS : Term Module Term Substitution -> Tuple{Term,Substitution} .
	eq fixBoxesLHS(LHS, M, T, Subst)
		= if fixBoxCardinalities(LHS, Subst) == error
			then (LHS, none)
			else fixIdsLHS(
							getTerm(
								metaReduce(
									upModule('UNBOXING, false),
									'unbox[fixBoxCardinalities(LHS, Subst)])), M, T, Subst)
			fi .
	ceq fixIdsLHS('`(_`,_`)[LHS, 'empty.Set`{Oid`}], M, T, Subst)
	  = if RT?:ResultTriple? == failure
			then (fixIds(LHS), none)
			else (fixIds(LHS), getSubstitution(RT?:ResultTriple?))
			fi
		if RT?:ResultTriple? := metaApply(mkIdRlModule(M, fixIds(LHS)), T, 'Id, rmBoxes(Subst), 0) .
	ceq fixIdsLHS('`(_`,_`)[LHS, T], M, T', Subst)
		= if RT?:ResultTriple? == failure
			then (fixIds(LHS), none)
			else fixBoxesLHS('__[fixIds(LHS), 'boxes[fixIds(T)]], M, T', getSubstitution(RT?:ResultTriple?)) fi
		if T =/= 'empty.Set`{Oid`}
		/\ RT?:ResultTriple? := metaApply(mkIdRlModule(M, fixIds(LHS)), T', 'Id, rmBoxes(Subst), 0) .

  op rmBoxes : Substitution -> Substitution .
	eq rmBoxes('Rels''':Configuration <- T ; Subst) = rmBoxes(Subst) .
	eq rmBoxes('Elts''':Configuration <- T ; Subst) = rmBoxes(Subst) .
	eq rmBoxes(Subst) = Subst [owise] .

	op fixBoxesRHS : Term Substitution -> Term .
	op fixIdsRHS : Term Substitution -> Term .
	eq fixBoxesRHS(RHS, Subst)
	 = if Subst == none
	 	 then RHS
		 else if fixBoxCardinalities(RHS, Subst) == error
					then RHS
					else fixIdsRHS(
									getTerm(
										metaReduce(
											upModule('UNBOXING, false),
											'unbox[fixBoxCardinalities(RHS, Subst)])), Subst)
					fi
	   fi .
	eq fixIdsRHS('`(_`,_`)[RHS, 'empty.Set`{Oid`}], Subst) = fixIds(RHS) .
	ceq fixIdsRHS('`(_`,_`)[RHS, T], Subst)
		= fixBoxesRHS('__[fixIds(RHS), 'boxes[fixIds(T)]], Subst)
		if T =/= 'empty.Set`{Oid`} .

	op mkIdRlModule : Module Term -> Module .
	eq mkIdRlModule(mod MN is IL sorts SS . SSDS OPDS Mbs Eqs Rls endm, T) = mod MN is IL sorts SS . SSDS OPDS Mbs Eqs (rl '`{_`}[T] => '`{_`}[T] [label('Id)] .) endm .

	op stringNL : NatList NzNat ~> String .
	eq stringNL(N NNL, N') = string(N, N') + "_" + stringNL(NNL, N') .
	eq stringNL(N, N') = string(N, N') .

	op fixIds : Term -> Term .
	op fixIdsAux : TermList -> TermList .
	eq fixIds(T) = fixIdsAux(T) .
	eq fixIdsAux(('vVar[C, T], TL)) = (C, fixIdsAux(TL)) .
	eq fixIdsAux(('vVar[V, T], TL))
		 = (qid(string(getName(V)) + "_" + stringNL(downNatList(T), 10) + ":" + string(getType(V))), fixIdsAux(TL)) .
	eq fixIdsAux(('vVar['_-_[T1, T2], T], TL))
		 = ('_-_[fixIdsAux('vVar[T1, T]), fixIdsAux('vVar[T2, T])]) .
	eq fixIdsAux(('vVar['_+_[T1, T2], T], TL))
		 = ('_+_[fixIdsAux('vVar[T1, T]), fixIdsAux('vVar[T2, T])]) .
	eq fixIdsAux(('vOid[V, T], TL))
		 = (qid(string(getName(V)) + "_" + stringNL(downNatList(T), 10) + ":Oid"), fixIdsAux(TL)) .
	eq fixIdsAux(('vOid['oid[V, T], T'], TL)) = ('oid[V, '__[T, T']], fixIdsAux(TL)) .
	eq fixIdsAux(('vId[V, T], TL))
		 = (qid(string(getName(V)) + "_" + stringNL(downNatList(T), 10) + ":Name"), fixIdsAux(TL)) .
	eq fixIdsAux(('vId['id[V, T], T'], TL)) = ('id[V, '__[T, T']], fixIdsAux(TL)) .
	eq fixIdsAux((F[TL], TL')) = (F[fixIdsAux(TL)], fixIdsAux(TL')) [owise] .
	eq fixIdsAux((V, TL)) = (V, fixIdsAux(TL)) .
	eq fixIdsAux((C, TL)) = (C, fixIdsAux(TL)) .
	eq fixIdsAux(empty) = empty .
	eq fixIdsAux(('box`[_`]`{_`}[T, T'], TL)) = ('box`[_`]`{_`}[fixIdsAux(T), fixIdsAux(T')], fixIdsAux(TL)) .

	op fixBoxCardinalities : Term Substitution -> [Term] .
	op fixBoxCardinalitiesAux : Term Substitution -> [Term] .
	op fixBoxCardinalitiesAux2 : Term Substitution -> [Term] .
	eq fixBoxCardinalities(('boxes[TL], TL'), Subst)
		 = ('boxes[fixBoxCardinalitiesAux(TL, Subst)], TL') [label foo] .
	eq fixBoxCardinalities(('<_:_|_>[TL], TL'), Subst)
		 = ('<_:_|_>[TL], fixBoxCardinalities(TL', Subst)) .
	eq fixBoxCardinalities((F[TL], TL'), Subst)
		 = (F[fixBoxCardinalities(TL, Subst)], fixBoxCardinalities(TL', Subst)) [owise] .
	eq fixBoxCardinalities((V, TL), Subst) = (V, fixBoxCardinalities(TL, Subst)) .
	eq fixBoxCardinalities((C, TL), Subst) = (C, fixBoxCardinalities(TL, Subst)) .
	eq fixBoxCardinalities(empty, Subst) = empty .

	eq fixBoxCardinalitiesAux(('box`[_`]`{_`}[T, TL], TL'), Subst)
		 = ('box`[_`]`{_`}[fixBoxCardinalitiesAux2(T, Subst), TL], fixBoxCardinalitiesAux(TL', Subst)) .
	eq fixBoxCardinalitiesAux((F[TL], TL'), Subst)
		 = (F[fixBoxCardinalitiesAux(TL, Subst)], fixBoxCardinalitiesAux(TL', Subst)) .
	eq fixBoxCardinalitiesAux((V, TL), Subst) = (V, fixBoxCardinalitiesAux(TL, Subst)) .
	eq fixBoxCardinalitiesAux((C, TL), Subst) = (C, fixBoxCardinalitiesAux(TL, Subst)) .
	eq fixBoxCardinalitiesAux(empty, Subst) = empty .

	eq fixBoxCardinalitiesAux2(V, (V <- T ; Subst)) = T .
	eq fixBoxCardinalitiesAux2(C, Subst) = C .
----	eq fixBoxCardinalitiesAux2(T, Subst) = error [owise] .

	op downNatList : Term -> NatList .
	eq downNatList('__[T, TL]) = downNatList(T) downNatList(TL) .
	eq downNatList('0.Zero) = 0 .
	eq downNatList('s_['0.Zero]) = 1 .
	ceq downNatList(F['0.Zero]) = trunc(rat(substr(string(F), 3, 2), 10)) if substr(string(F), 0, 3) = "s_^" .
endm

view Object from TRIV to CONFIGURATION is
	sort Elt to Object .
endv
