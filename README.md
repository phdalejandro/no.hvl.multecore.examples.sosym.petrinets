# README #

In this repository one can find all the source files that are necessary to run the experiments detailed in the submitted article for the 2020 Theme Issue on "Multi-level Modelling" on the Journal Software and Systems Modeling (SoSyM).

### Required tools ###

In order to reproduce the experiments, one need to have installed MultEcore and Maude tools:

The complete MultEcore tool is composed by two parts, which are available in [MultEcore repository](https://bitbucket.org/phd-fernando/no.hvl.multecore) and in [MultEcore MCMTs repository](https://bitbucket.org/phdalejandro/no.hvl.multecore.mcmt/src/master/).

TheMaude System is available in the [Maude website](http://maude.cs.illinois.edu/w/index.php/The_Maude_System).


### MultEcore files ###

Once MultEcore has been installed, and in order to be able to generate the complete Maude specification from the MultEcore files, one need to import both
[no.hvl.multecore.examples.sosym.petrinets](https://bitbucket.org/phdalejandro/no.hvl.multecore.examples.sosym.petrinets/src/master/no.hvl.multecore.examples.sosym.petrinets/) and
[no.hvl.multecore.mcmt.examples.sosym.petrinets](https://bitbucket.org/phdalejandro/no.hvl.multecore.examples.sosym.petrinets/src/master/no.hvl.multecore.mcmt.examples.sosym.petrinets/)
projects into the workspace.

By activating MultEcore into the first of these two projects, the MLM capabilities are available. Since the MCMT rules to run the gas-station are ready to be transformed as well, we can directly generate the specification.
(To load the MCMTs, we suggest to make a change and save the **petrinets.mcmt** file.

If we right click in the **no.hvl.multecore.examples.sosym.petrinets** project, there must appear a contextual menu of the available MultEcore tools as the figure below shows:


![MultEcore contextual menu](https://i.imgur.com/Xj0Rjpm.png)


Please, note that even though there might be other options such as _Maude to MultEcore_ or _Create CPN Editor_, these are still under development, so we suggest to not use them.

Our option is MultEcore to Maude. By clicking there, there will appear two consecutive wizards to select first, the model on which we wan to run the rules (**gas-station**) and which rules to be actually transformed.
The figure below shows both wizards, and the options (1) and (2) that should be selected for the **gas-station** case study:

![MultEcore to Maude options](https://i.imgur.com/kgpteKw.png)

After clicking **OK** in the second wizard, there should appear a new folder within our **no.hvl.multecore.examples.sosym.petrinets** project, named **maude-outputs**, with the complete maude specification.

### Maude files ###

Even though we have detailed instructions to generate the complete Maude representation, we also provide the already generated files in [maude-outputs](https://bitbucket.org/phdalejandro/no.hvl.multecore.examples.sosym.petrinets/src/master/maude-outputs/) directory.
Within it, one can find two folders with two Maude specifications in which both, contain the same files structure:

"2-tuple.maude", "3-tuple.maude" and "mlm-core.maude" contain the required signatures to run a MLM hierarchy in Maude.

"generic-petri-net.maude" has all the transformed MCMT rules

"gas-station.maude" is the representation of the Petri-nets multilevel modelling hierarchy where the bottom-most level corresponds to the initial state of the model.

The two directories provided are meant for:

 - **generated-from-MultEcore**: This one corresponds to the files that are automatically generated from the MultEcore-Maude transformer.
 	By defining some simulation/execution commands, one can explore different runs of the "gas-station.maude". We provide an example of a trace at the end of "gas-station.maude" file.
 - **constant-versions-of-rules**: Notice that the files structure is the same. The difference here is that we provide a version of the rules where several variables have been replace to equivalent constants.
    This is because the reachability analysis we have performed explored the state space. While using variables promotes flexibility and reusability, they also carry a cost on efficiency.
	Thus, we reduce the cost and times of the analysis tool by substituting some of these variables. All the experiments carried on and explained in the paper can be found and run in the "gas-station-maude" version file in this directory.
	Note that they are commented, removing each comment (the comment has the form: ---( <property> )) makes the property checkeable.
    Also, we incorporate the abstraction (that makes the state space finite) specification together with some temporal properties verified, described in Section 4.3 of the article, in the "abstraction.maude" file.
	Ultimately, we provide the complete output resulting of the execution of the properties shown in Sections 4.2 and 4.3.